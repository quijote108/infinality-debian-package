#!/bin/bash

# INFINALITY="http://www.infinality.net/fedora/linux/zips/fontconfig-infinality-1-20130104_1.tar.bz2"
INFINALITY="http://distro.ibiblio.org/slitaz/packages/cooking/fontconfig-infinality-1-20130104_1.tazpkg"

if [ ! -f ${INFINALITY##*/} ]; then
  wget "${INFINALITY}"
fi

if [ -d fontconfig-infinality-1 ]; then
  rm -rvf fontconfig-infinality-1
fi

if [ -d extracted ]; then
	rm -rvf extracted
fi

mkdir extracted

mv fontconfig-infinality-1-20130104_1.tazpkg extracted/fontconfig-infinality-1-20130104_1.tazpkg.gz

cd extracted

gunzip fontconfig-infinality-1-20130104_1.tazpkg.gz

cpio -id < fontconfig-infinality-1-20130104_1.tazpkg.gz

lzcat fs.cpio.lzma | cpio -idm

cd ..

mkdir fontconfig-infinality-1

cd fontconfig-infinality-1

cp -rv ../debian/ .

cp -rv ../extracted/fs/etc/fonts/* .

cp -rv ../extracted/fs/usr/bin/infctl.sh infinality/

debuild -us -uc -b
